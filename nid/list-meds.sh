#!/usr/bin/env bash

is_ned () {
    PMDXML=$1
    NED_COMP_LINES=$(xmlstarlet sel -N x=http://tail-f.com/ns/ncs-packages -t -v '/x:ncs-package/x:component/x:ned' -nl ${PMDXML} | wc -l)
    if [ ${NED_COMP_LINES} -gt 1 ]; then
        true
    else
        false
    fi
}

for PKG_DIR in $(ls packages/); do
    if [ -f packages/${PKG_DIR}/src/package-meta-data.xml.in ]; then
        if is_ned "packages/${PKG_DIR}/src/package-meta-data.xml.in"; then
            echo ${PKG_DIR}
        fi
    elif [ -f packages/${PKG_DIR}/package-meta-data.xml ]; then
        if is_ned "packages/${PKG_DIR}/package-meta-data.xml"; then
            echo ${PKG_DIR}
        fi
    fi
done
