module ha-portier {
  yang-version "1.1";

  namespace "http://gitlab.com/nso-developer/ha-portier";
  prefix ha-portier;

  import ietf-inet-types {
    prefix inet;
  }
  import tailf-common {
    prefix tailf;
  }
  import tailf-ncs {
    prefix ncs;
  }

  description
    "HA portier uses the HA mode for conditioning listening on ports and
    forwarding connections to NSO";

  revision 2022-03-15 {
    description
      "Initial revision.";
  }

  augment "/ncs:high-availability" {
    container "portier" {
      leaf enabled {
        type boolean;
        default true;
      }

      list port-mapping {
        description "Ports to forward. If this list is empty, a default list of"
          + "port mappings is used:"
          + "  PROTO  PORT  HA PORT"
          + "  tcp    22    10022"
          + "  tcp    80    10080"
          + "  tcp    443   10443"
          + "  tcp    830   10830"
          + "  tcp    4334  14334"
          + ""
          + "TCP is the only currently supported protocol";

        key port;
        unique ha-port;
        leaf port {
          description "Base port to forward traffic to";
          type uint16;
        }

        leaf ha-port {
          description "HA port to listen on and forward traffic from";
          type uint16;
        }
      }

      action restart {
        tailf:actionpoint "ha-portier-restart";
        output {
          leaf result {
            type string;
          }
        }
      }

      action emergency-stop {
        tailf:actionpoint "ha-portier-emergency-stop";
        output {
          leaf result {
            type string;
          }
        }
      }
    }
  }
}
