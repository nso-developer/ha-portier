#!/usr/bin/env python3

import atexit
import logging
import signal
import subprocess
import sys

import ncs

from ha_portier import background_process

# Ports to forward
# protocol, listen port, forward to port
PORT_FWDS = [
    ('tcp', 10022, 22),     # SSH
    ('tcp', 10080, 80),     # HTTP
    ('tcp', 10443, 443),    # HTTPS
    ('tcp', 10830, 830),    # NETCONF
    ('tcp', 14334, 4334),   # NETCONF call-home
]

def run_fwds():
    log = logging.getLogger()
    log.info(f"HA Portier starting")

    # Our processes indexed by listening port
    ps = {}

    def cleanup():
        """Stop all of our subprocesses
        First send SIGTERM to allow clean shutdown, wait a tiny bit and then
        send SIGKILL
        """

        for lp, p in ps.items():
            try:
                p.terminate()
            except ProcessLookupError:
                pass

        for lp, p in ps.items():
            try:
                r = p.wait(0.1)
            except ProcessLookupError:
                pass

        for lp, p in ps.items():
            try:
                p.kill()
            except ProcessLookupError:
                pass

    def sig_handler(signo, frame):
        cleanup()
        sys.exit(0)

    atexit.register(cleanup)
    signal.signal(signal.SIGTERM, sig_handler)

    # Per default, use pre-defined list of ports
    port_fwds = PORT_FWDS

    # Check if port mapping is configured and use it
    with ncs.maapi.single_read_trans('ha-resolver', 'system', db=ncs.OPERATIONAL) as oper_trans_read:
        root = ncs.maagic.get_root(oper_trans_read)
        if len(root.high_availability.portier.port_mapping) > 0:
            port_fwds = []
        for p in root.high_availability.portier.port_mapping:
            port_fwds.append(('tcp', p.ha_port, p.port))

    for proto, listen_port, dst_port in port_fwds:
        log.info(f"Listening on {proto}/{listen_port} and forwarding to {dst_port}")
        cmd = ["socat",
               f"{proto.upper()}-LISTEN:{listen_port},fork,reuseaddr",
               f"{proto.upper()}:127.0.0.1:{dst_port}"]
        p = subprocess.Popen(cmd, stdout=subprocess.DEVNULL,
                             stderr=subprocess.DEVNULL, universal_newlines=True,
                             start_new_session=True)
        ps[listen_port] = p

    while True:
        for listen_port, p in ps.items():
            try:
                if p.wait(timeout=1) is not None:
                    log.warning(f"Process {p} for listening on {listen_port} is dead, exiting..")
                    return
            except subprocess.TimeoutExpired:
                pass


class Main(ncs.application.Application):
    def setup(self):
        self.worker = background_process.Process(self, run_fwds, [], config_path='/high-availability/portier/enabled', ha_when='master')
        self.register_action('ha-portier-restart', background_process.RestartWorker, init_args=self.worker)
        self.register_action('ha-portier-emergency-stop', background_process.EmergencyStop, init_args=self.worker)
        self.worker.start()

    def teardown(self):
        self.worker.stop()
