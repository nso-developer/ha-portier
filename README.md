# HA Portier

HA Portier simplifies deploying NSO in conjunction with a network traffic load
balancer. This is an alternative to using the virtual IP function in the NSO HCC
package. In particular in cloud deployments, where it is commonly not possible
to use a virtual IP that is moved across availability zones or similar, this can
be useful as it allows using a common load balancing function to achieve a
similar end result; the "virtual IP" is attached to the load balancer.

Network traffic load balancers commonly implement host liveness detection. The
most fundamental method of determining liveness of a host is to connect to the
traffic bearing port and if the connection succeeds, the host is considered live
and healthy whereas if the connection is rejected, through a TCP RST, the load
balancer will consider the host dead. Traffic is then spread over all live nodes.

By conditionally listening on a port only when we are the primary HA node
(`master`), we can effectively signal the load balancer which node should take
traffic. This package helps you do that.

NSO always listens on its configured ports, for example NETCONF on TCP/830
regardless if HA is enabled or not and regardless of in which HA mode we
currently are. The HA Portier monitors the HA-mode of NSO and sets up a TCP
proxy forwarder on a alternate listening port and vice versa; if we are not
primary node, the TCP proxy is stopped. The conditional ports use an offset
of 10000. For example, the conditional NETCONF listening port is 10830.

# Prerequisites

Install `socat`, which is used for the actual TCP forwarding function.

# Usage

Loading the package in NSO is enough to make it work. HA portier can be disabled
via `/high-availability/portier/enabled`. The background process that in turn
runs socat can be restarted with `request high-availability portier restart` or
stopped immediately with `request high-availability portier emergency-stop`.

Per default, the following ports will be forwarded:
- TCP 10022 -> 22
- TCP 10080 -> 80
- TCP 10443 -> 443
- TCP 10830 -> 830
- TCP 14334 -> 4334

You can configure a different port-mapping in
`/high-availability/portier/port-mapping`, for example:

```
admin@ncs% show high-availability portier
port-mapping 2022 {
    ha-port 10022;
}
port-mapping 8080 {
    ha-port 10080;
}
[ok][2022-09-19 10:22:36]

[edit]
admin@ncs%
```

Note that the configured port-mapping will replace the default list of ports.
You cannot just change one individual mapping without redefining the whole list
of port mappings.

NOTE: you must restart ha-portier manually if you make any changes to the
configuration, i.e. run `request high-availability portier restart`.

## Configure load balancer

Add all NSO nodes to the load balancer and configure the appropriate port with
an offset of 10000. For example

- NSO nominal primary: 192.0.2.1 port 10830
- NSO nominal secondary: 192.0.2.2 port 10830
- NSO nominal failover-primary: 192.0.2.3 port 10830

Only the node that is currently the primary (`HA-mode=master`) will respond to
port 10830 and consequently, the load balancer will forward all traffic to it.

# Caveats

For NSO using the rule based consensus algorithm in 5.7 and later, the node with
nominal-role `master` will enter HA-mode=master in read-only mode until a
secondary node has joined. We would like to reflect this by not taking traffic
until we are in read-write mode, but it is not possible to subscribe to this
event.

